package kg.lesson56lab.plannedtasks.service;

import kg.lesson56lab.plannedtasks.dto.NewUserDTO;
import kg.lesson56lab.plannedtasks.dto.ResponceDTO;
import kg.lesson56lab.plannedtasks.dto.UserDTO;
import kg.lesson56lab.plannedtasks.model.User;
import kg.lesson56lab.plannedtasks.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserService {
    @Autowired
    UserRepository users;

    public Slice<UserDTO> findUsers(Pageable pageable) {
        var slice = users.findAll(pageable);
        return slice.map(UserDTO::from);
    }

    public ResponceDTO addUser(NewUserDTO data){
        try{
            if (!users.existsByEmail(data.getEmail())){
                var newUser = User.builder()
                        .email(data.getEmail())
                        .name(data.getName())
                        .password(new BCryptPasswordEncoder().encode(data.getPassword()))
                        .build();
                users.save(newUser);
                return ResponceDTO.from("201", newUser.getId(),"Успешно зарегистрирован!");
            } else {
                return ResponceDTO.from("400","null", "Пользователь уже существует");
            }
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "null", "Ошибка!");
        }
    }

}
