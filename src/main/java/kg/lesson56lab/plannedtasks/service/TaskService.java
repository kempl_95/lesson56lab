package kg.lesson56lab.plannedtasks.service;

import kg.lesson56lab.plannedtasks.dto.*;
import kg.lesson56lab.plannedtasks.model.Task;
import kg.lesson56lab.plannedtasks.model.User;
import kg.lesson56lab.plannedtasks.repository.TaskRepository;
import kg.lesson56lab.plannedtasks.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class TaskService {
    @Autowired
    TaskRepository tasks;

    public Slice<TaskDTO> findUserTasks(Authentication authentication, Pageable pageable) {
        var user = (User)authentication.getPrincipal();
        var slice = tasks.findAllByUserId(user.getId(), pageable);
        return slice.map(TaskDTO::from);
    }

    public ResponceDTO addTask(Authentication authentication, NewTaskDTO data){
        try{
            if (!tasks.existsByTitle(data.getTitle())){
                User user = (User)authentication.getPrincipal();
                var newTask = Task.builder()
                        .title(data.getTitle())
                        .description(data.getDescription())
                        .plannedDate(data.getPlannedDate())
                        .status("Новая")
                        .user(user)
                        .build();
                tasks.save(newTask);
                return ResponceDTO.from("201", newTask.getId(),"Задача успешно создана!");
            } else {
                return ResponceDTO.from("400","null", "Задача с таким заголовком уже есть");
            }
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "null", "Ошибка!");
        }
    }
    public ResponceDTO changeStatus(String id, Authentication authentication){
        try{
            User user = (User)authentication.getPrincipal();
            var thisTask = tasks.getById(id);
            if (thisTask.getUser().getId().equals(user.getId())){
                if (!thisTask.getStatus().equals("Завершена")){
                    if(thisTask.getStatus().equals("Новая")){
                        thisTask.setStatus("В работе");
                        tasks.save(thisTask);
                        return ResponceDTO.from("201", thisTask.getId(),"Статус задачи изменен на 'В работе'");
                    }else{
                        thisTask.setStatus("Завершена");
                        tasks.save(thisTask);
                        return ResponceDTO.from("201", thisTask.getId(),"Статус задачи изменен на 'Завершена'");
                    }
                } else {
                    return ResponceDTO.from("400","null", "Задача уже завершена. Статус не изменен");
                }
            }else {
                return ResponceDTO.from("400","null", "Задача не пренадлежит пользователю");
            }
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "null", "Ошибка!");
        }
    }

    public TaskDTO findUserTasksById(Authentication authentication, String id) {
        User user = (User)authentication.getPrincipal();
        Task errorTask = new Task("null", "error", "error", "error", user, "error");
        try{
            var thisTask = tasks.getById(id);
            if (thisTask.getUser().getId().equals(user.getId())){
                return TaskDTO.from(thisTask);
            }else {
                return TaskDTO.from(errorTask);
            }
        } catch (IndexOutOfBoundsException | NullPointerException ex){
            return TaskDTO.from(errorTask);
        }
    }

}
