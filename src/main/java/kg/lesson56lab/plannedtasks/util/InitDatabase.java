package kg.lesson56lab.plannedtasks.util;

import kg.lesson56lab.plannedtasks.model.Task;
import kg.lesson56lab.plannedtasks.model.User;
import kg.lesson56lab.plannedtasks.repository.TaskRepository;
import kg.lesson56lab.plannedtasks.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class InitDatabase {

    private static final Random r = new Random();
    private static final int userQty = 20;
    private static final int taskQty = 50;

    @Bean
    CommandLineRunner init(UserRepository users, TaskRepository tasks) {
        return (args) -> {
            tasks.deleteAll();
            users.deleteAll();

            //Добавляю пользователей
            List<User> userList = new ArrayList<>();
            userList.add(User.admin());
            userList.add(User.guest());
            for (int i=0; i<userQty; i++){
                userList.add(User.random());
            }
            users.saveAll(userList);

            //Добавляю Задачи
            List<Task> taskList = new ArrayList<>();
            for (int i=0; i<taskQty; i++){
                taskList.add(Task.random(userList.get(r.nextInt(userList.size()))));
            }
            tasks.saveAll(taskList);
            System.out.println("=================== DONE ===================");
            System.out.println();
            System.out.println("Test users: ");
            System.out.println("| Email: admin@gmail.com | password: admin |");
            System.out.println("| Email: guest@gmail.com | password: guest |");
        };
    }
}
