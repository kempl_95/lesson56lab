package kg.lesson56lab.plannedtasks.model;

import kg.lesson56lab.plannedtasks.util.Generator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="tasks")
public class Task {

    private static final Random r = new Random();

    public static Task random(User user) {
        return builder()
                .title(Generator.makeName())
                .description(Generator.makeDescription())
                .plannedDate(LocalDateTime.now().plusDays(r.nextInt(30)+1).toString())
                .user(user)
                .status(getRandomStatus(r.nextInt(3)+1))
                .build();
    }

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String title;
    private String description;
    private String plannedDate;
    @DBRef
    private User user;
    private String status;

    private static String getRandomStatus(int i){
        switch (i){
            default: return "Новая";
            case 2:
                return "В работе";
            case 3:
                return "Завершена";

        }
    }
}
