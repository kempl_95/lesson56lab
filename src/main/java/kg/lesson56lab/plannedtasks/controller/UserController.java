package kg.lesson56lab.plannedtasks.controller;

import kg.lesson56lab.plannedtasks.annotations.ApiPageable;
import kg.lesson56lab.plannedtasks.dto.NewUserDTO;
import kg.lesson56lab.plannedtasks.dto.ResponceDTO;
import kg.lesson56lab.plannedtasks.dto.UserDTO;
import kg.lesson56lab.plannedtasks.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiPageable
    @GetMapping
    public Slice<UserDTO> findUsers(@ApiIgnore Pageable pageable) {
        return userService.findUsers(pageable);
    }
    //регистрация
    @PostMapping(path = "/registration", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO addSubscription(@RequestBody NewUserDTO data) {
        return userService.addUser(data);
    }

}
