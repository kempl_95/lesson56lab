package kg.lesson56lab.plannedtasks.controller;

import kg.lesson56lab.plannedtasks.annotations.ApiPageable;
import kg.lesson56lab.plannedtasks.dto.NewTaskDTO;
import kg.lesson56lab.plannedtasks.dto.NewUserDTO;
import kg.lesson56lab.plannedtasks.dto.ResponceDTO;
import kg.lesson56lab.plannedtasks.dto.TaskDTO;
import kg.lesson56lab.plannedtasks.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    @Autowired
    private TaskService taskService;


    //Задачи текущего пользователя
    @GetMapping(path = "/myTasks")
    public Slice<TaskDTO> findAuthUserTasks(Authentication authentication, @ApiIgnore Pageable pageable) {
        return taskService.findUserTasks(authentication, pageable);
    }

    //Добавление задачи
    @PostMapping(path = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO addTask(Authentication authentication, @RequestBody NewTaskDTO data) {
        return taskService.addTask(authentication, data);
    }

    //Изменение статуса
    @PostMapping(path = "/change/{id}")
    public ResponceDTO changeStatus(Authentication authentication, @PathVariable String id) {
        return taskService.changeStatus(id, authentication);
    }

    //Задача текущего пользователя по ID
    @GetMapping(path = "/myTasks/{id}")
    public TaskDTO findAuthUserTasks(Authentication authentication, @PathVariable String id) {
        return taskService.findUserTasksById(authentication, id);
    }
}
