package kg.lesson56lab.plannedtasks.dto;

import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class ResponceDTO {
    private String answerCode;
    private String id;
    private String answerMessage;

    public static ResponceDTO from(String answerCode, String id, String answer) {
        return builder()
                .answerCode(answerCode)
                .id(id)
                .answerMessage(answer)
                .build();
    }
}
