package kg.lesson56lab.plannedtasks.dto;

import kg.lesson56lab.plannedtasks.model.Task;
import kg.lesson56lab.plannedtasks.model.User;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.UUID;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class TaskDTO {

    public static TaskDTO from(Task task) {
        return builder()
                .id(task.getId())
                .title(task.getTitle())
                .plannedDate(task.getPlannedDate())
                .status(task.getStatus())
                .build();
    }

    private String id;
    private String title;
    private String plannedDate;
    private String status;

}
