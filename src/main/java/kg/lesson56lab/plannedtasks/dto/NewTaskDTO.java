package kg.lesson56lab.plannedtasks.dto;

import kg.lesson56lab.plannedtasks.model.Task;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class NewTaskDTO {

    public static NewTaskDTO from(Task task) {
        return builder()
                .title(task.getTitle())
                .description(task.getDescription())
                .plannedDate(task.getPlannedDate())
                .build();
    }
    private String title;
    private String description;
    private String plannedDate;
}
