package kg.lesson56lab.plannedtasks.dto;

import kg.lesson56lab.plannedtasks.model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class NewUserDTO {

    public static NewUserDTO from(User user) {
        return builder()
                .email(user.getEmail())
                .name(user.getName())
                .password(user.getPassword())
                .build();
    }

    private String email;
    private String name;
    private String password;

}
