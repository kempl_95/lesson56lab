package kg.lesson56lab.plannedtasks.repository;

import kg.lesson56lab.plannedtasks.model.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TaskRepository extends PagingAndSortingRepository<Task, String> {
    public Page<Task> findAllByUserId(String id, Pageable pageable);
    public boolean existsByTitle(String title);
    public Task getById(String id);
}
