package kg.lesson56lab.plannedtasks.repository;


import kg.lesson56lab.plannedtasks.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, String> {
    public Optional<User> getByEmail(String s);
    public boolean existsByEmail(String email);
}
