package kg.lesson56lab.plannedtasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlannedtasksApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlannedtasksApplication.class, args);
    }

}
